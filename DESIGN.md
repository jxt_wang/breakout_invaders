# DESIGN

Written by Justin Wang (*jtw57*)

-----
### Design Goals
The overarching goal driving the design was to achieve a modular game with expandability at its heart. In order to achieve this, the following considerations were made:
>* Goal One: Express modularity through appropriate method delegation and inheritance
>* Goal Two: Maintain code readability
>* Goal Three: Ensure proper interactivity (gameplay)
>* Goal Four: Provide a framework for easy adding of additional features
-----
### Adding New Features
Depending on the nature of the feature, it can be added in several ways:

1. Adding Level-related features
2. Adding Paddle-related features
    * Will need to interact with the Defender class
    * Can extend Entity to ensure uniformity of collision checking requirements
3. Adding Invader-related features
4. Extending current features

Any features that require collision checking or stat updating will need to interact with the Level and CollisionEngine classes.
-----
### Major Design Choices
-----
### Assumptions/Decisions Made

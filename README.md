NOTE: I deleted the original repository that was posted on January 14th as it contained an older version of the project.
------
About
------
> Author: Justin Wang <br />
> Breakout Game V1.0 <br />
> Developed for CS308 <br />

A variation of the original Breakout game that implements elements from another classic - Space Invaders. <br />

> Date Started: January 15<br />
> Date Finished: January 23<br />

Main Program: GameEngine.java
Configuration File: Adjust any 'magic numbers' for testing.<br />

Data
------
<br />
All data/img files are included in their respective folders
> data.txt contains level data<br />
> MunroSmall.ttf contains font data<br />

All relevant key bindings are explained on the splash screen.<br />

Invader Types
------
<br />
Feel free to change the data.txt file using the following conventions:

> "ST": Standard<br />
> "SH": Shield<br />
> "AS": Assault<br />
> "SU": Support<br />
> "SD": Supply Drop<br />

Standard Invader (White): Invader that requires one hit to be destroyed. Does not have the ability to fire.<br /><br />

Assault Invader (Red): Like the standard invader, it only requires one hit to be destroyed. However, this
invader has the ability to fire small projectiles towards the player. These projectiles move vertically and can
only be deflected by a player's shield. Its projectiles can only harm the player's paddle.<br /><br />

Shield Invader (Blue): This invader carries a shield that prevents it from being killed by a direct hit to the
side facing the player. However, if the invader is hit on any other side, it is destroyed.<br /><br />

Support Invader (Green): Invader that requires 3 hits to be killed.<br /><br />

Supply Drops: These blocks contain one of the various power ups listed above. When hit, the ability is instantly
activated.<br /><br />

Missing Features
------
> PowerUps: Artillery powerup has not been implemented.

Gameplay Footage A
------
![sampleA](img/sampleA.gif)
<br />
Gameplay Footage B
------
![sampleB](img/sampleB.gif)
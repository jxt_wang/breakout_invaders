/**
 * Created by Justin Wang on 1/15/2017.
 * PowerUp Class
 *
 **/

import config.Configuration;
import config.InvaderClass;
import config.PowerUpClass;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.concurrent.ThreadLocalRandom;

//TODO: USE DEFENDER POINTER TO UPDATE ABILITIES

public class PowerUp extends Invader{

    private Defender mainDefender;

    private PowerUpClass powerUpClass;

    private Text powerUpText;

    private boolean setPowerUp;

    public PowerUp(Defender mainDefender, Group invaderGroup, Scene primaryScene, int rowCount, int numInRow, int rowNum) {
        super(invaderGroup, InvaderClass.POWERUP, primaryScene, rowCount, numInRow, rowNum);
        this.mainDefender = mainDefender; this.setPowerUp = false;
    }

    public PowerUp(Defender mainDefender, Group invaderGroup, Scene primaryScene, int rowCount, int numInRow, int rowNum, PowerUpClass powerUpClass) {
        super(invaderGroup, InvaderClass.POWERUP, primaryScene, rowCount, numInRow, rowNum);
        reRender(powerUpClass);
        this.mainDefender = mainDefender; this.setPowerUp = false;
    }

    public void selectPower(int randGen){
        switch (randGen){
            case 1: case 2: case 3: case 4:
                powerUpClass = PowerUpClass.INVADERCIDE; break;
            case 5: case 6: case 7:
                powerUpClass = PowerUpClass.EMP; break;
            case 8:
                powerUpClass = PowerUpClass.SHIELD; break;
            default:
                powerUpClass = PowerUpClass.NONE; break;
        }
    }

    public void reRender(PowerUpClass powerUpClass){
        this.powerUpClass = powerUpClass;
        powerUpText.setText(powerUpClass.toString());
    }

    @Override
    public void render(){
        super.render();
        if(!setPowerUp){this.selectPower(ThreadLocalRandom.current().nextInt(1, 9));}
        powerUpText = new Text();
        powerUpText.setFont(Font.loadFont(getClass().getClassLoader().getResourceAsStream(Configuration.FONT), 10));
        powerUpText.setX(this.getMinX());
        powerUpText.setY(this.getMinY());
        powerUpText.setText(powerUpClass.toString());
        this.getRootNode().getChildren().add(powerUpText);
    }

    @Override
    public void checkCollision(Entity entityX) {
        super.checkCollision(entityX);
    }

    @Override
    public void explode(){
        super.explode();
        this.getRootNode().getChildren().remove(powerUpText);
        mainDefender.enablePowerUp(powerUpClass);
    }

}

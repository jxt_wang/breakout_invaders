/**
 * Created by Justin Wang on 1/15/2017.
 * Abstract Class For All Game Characters
 *
 **/

import com.sun.javafx.geom.BaseBounds;
import com.sun.javafx.geom.transform.BaseTransform;
import com.sun.javafx.jmx.MXNodeAlgorithm;
import com.sun.javafx.jmx.MXNodeAlgorithmContext;
import com.sun.javafx.sg.prism.NGNode;
import config.Configuration;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Entity extends Node{

    /*Display Elements*/
    private Image displayImg;
    private Node displayNode;

    /*Velocity Values*/
    private double xVelocity;
    private double yVelocity;

    /*Position Values*/
    private double startX;
    private double startY;

    /*Entity Health*/
    private int HP;
    private boolean isDead;

    /*Root Node*/
    private Group rootNode;

    /*CONSTRUCTORS*/

    public Entity(Group rootNode, double startX, double startY, double xVelocity, double yVelocity){

        this.HP = Configuration.STANDARD_HP;

        this.startX = startX;
        this.startY = startY;

        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;

        this.rootNode = rootNode;
        this.displayNode = new ImageView();

    }

    public Entity(Group rootNode){
        this(rootNode, 0, 0, 0, 0);
    }

    public Entity(){
        this.HP = Configuration.STANDARD_HP; this.displayNode = new ImageView();
    }

    /*PRIMARY FUNCTIONS*/

    public void setImage(Image displayImg){

        this.displayImg = displayImg;
        this.displayNode = new ImageView(this.displayImg);

    }

    public void setImage(String fileName){
        setImage(new Image(this.getClass().getClassLoader().getResourceAsStream(fileName)));
    }

    public void setPosition(double xPosition, double yPosition){
        this.startX = xPosition;
        this.startY = yPosition;
        this.displayNode.setTranslateX(xPosition);
        this.displayNode.setTranslateY(yPosition);
    }

    public void setPosition(){
        this.setPosition(this.startX, this.startY);
    }

    public void addVelocity(double xVelocity, double yVelocity){
        this.xVelocity += xVelocity;
        this.yVelocity += yVelocity;
    }

    public void setVelocity(double xVelocity, double yVelocity){
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
    }

    public void setNode(Node displayNode){ this.displayNode = displayNode; }

    public void setHP(int HP){ this.HP = HP; }

    public void removeHP(int HP){ this.HP -= HP; }

    public void explode(){ this.isDead = true; }

    public void update(){

        this.getNode().setTranslateX(this.getNode().getTranslateX() + this.getVelocityX());
        this.getNode().setTranslateY(this.getNode().getTranslateY() + this.getVelocityY());

    }

    public void setDead(boolean isDead){ this.isDead = isDead; }

    public void setVelocityX(double xVelocity){ this.xVelocity = xVelocity; }

    public void setVelocityY(double yVelocity){ this.yVelocity = yVelocity; }

    /*RETURN STATEMENTS*/

    public int getHP(){ return HP; }

    public boolean isDead(){ return isDead; }

    public double getMinX(){ return this.getNode().getBoundsInParent().getMinX();}

    public double getMinY(){ return this.getNode().getBoundsInParent().getMinY();}

    public double getMaxX(){ return this.getNode().getBoundsInParent().getMaxX();}

    public double getMaxY(){ return this.getNode().getBoundsInParent().getMaxY();}

    public double getWidth(){ return this.getNode().getBoundsInParent().getWidth(); }

    public double getHeight(){ return this.getNode().getBoundsInParent().getHeight(); }

    public double getVelocityX(){ return xVelocity; }

    public double getVelocityY(){ return yVelocity; }

    public Node getNode(){return displayNode;}

    public Group getRootNode(){ return rootNode; }

    /*ABSTRACT FUNCTIONS*/

    public abstract void render();

    public abstract void checkCollision(Entity entityX);

    public abstract void healthCheck();

}
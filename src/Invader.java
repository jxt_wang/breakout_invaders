/**
 * Created by Justin Wang on 1/15/2017.
 * Invader Class
 *
 **/

import config.Configuration;
import config.InvaderClass;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Invader extends Entity{

    /*Scene Element*/
    private Scene primaryScene;

    /*Classification*/
    private InvaderClass invaderClass;

    /*Characteristics*/
    private Color invaderColor;

    /*Row Count*/
    private int rowCount;
    private int numInRow;
    private int rowNum;

    public Invader(Group invaderGroup, InvaderClass invaderClass, Scene primaryScene, int rowCount, int numInRow, int rowNum) {
        super(invaderGroup);
        this.invaderClass = invaderClass;
        this.primaryScene = primaryScene;
        this.rowCount = rowCount; this.numInRow = numInRow; this.rowNum = rowNum;
        invaderType(invaderClass);
        render();
        this.getRootNode().getChildren().add(this.getNode());
    }

    public void invaderType(InvaderClass invaderClass){

        invaderColor = Color.rgb(0,0,0);

        switch (invaderClass){
            case STANDARD:
                invaderColor = Configuration.DARK_GREY; break;
            case SHIELD:
                invaderColor = Configuration.BLUE; break;
            case ASSAULT:
                invaderColor = Configuration.RED; break;
            case SUPPORT:
                invaderColor = Configuration.GREEN; this.setHP(Configuration.SUPPORT_HP); break;
            case POWERUP:
                invaderColor = Configuration.BRIGHT_BLUE; break;
            default:
                setDead(true); break;
        }

    }

    public InvaderClass invaderClass(){
        return invaderClass;
    }

    public boolean attack(){ return (invaderClass == InvaderClass.ASSAULT); }

    /*PARENT FUNCTIONS*/

    @Override
    public void explode(){
        super.explode();
        this.getRootNode().getChildren().remove(this.getNode());
    }

    @Override
    public void render() {

        if(!isDead()) {
            double widthX = (primaryScene.getWidth() * Configuration.WIDTH_RATIO) / (rowCount);
            double heightY = (primaryScene.getHeight() * Configuration.HEIGHT_RATIO) / (rowCount);

            Rectangle displayRect = new Rectangle() {
                {
                    setWidth(widthX);
                    setHeight(heightY);
                    setFill(invaderColor);
                }
            };

            //TODO: implement pictures
            //ImageView displayRect = new ImageView(new Image(getClass().getClassLoader().getResourceAsStream(Configuration.DEFENDER)));

            this.setNode(displayRect);
            this.setPosition((numInRow - 1) * (widthX + Configuration.BUFFER), (primaryScene.getWidth() * (1 - Configuration.WIDTH_RATIO) / rowCount) * rowNum);
        }
    }

    @Override
    public void checkCollision(Entity entityX) {

        if(entityX instanceof Ball){

            if(entityX.getNode().getBoundsInParent().intersects(this.getNode().getBoundsInParent()) && ((Ball) entityX).isReleased()){

                if(this.getMinY() > entityX.getMinY() && entityX.getVelocityY() > 0){
                    ((Ball) entityX).invY();
                    healthCheck();
                }
                if((this.getMinX() > entityX.getMinX() && entityX.getVelocityX() > 0) || (this.getMaxX() < entityX.getMaxX() && entityX.getVelocityX() < 0)){
                    ((Ball) entityX).invX();
                    healthCheck();
                }
                if(this.getMaxY() < entityX.getMaxY() && entityX.getVelocityY() < 0){
                    ((Ball) entityX).invY();
                    if(invaderClass != InvaderClass.SHIELD){ healthCheck(); };
                }
                if(((Ball) entityX).getInvadercide()){
                    ((Ball) entityX).setInvader(this.invaderClass);
                    ((Ball) entityX).setInvadercide(false);
                }

            }

        }

    }

    public void darkenColor(){

        int newRed = (int) (invaderColor.getRed()*255) + Configuration.COLOR_CHANGE;
        int newGreen = (int) (invaderColor.getGreen()*255) + Configuration.COLOR_CHANGE;
        int newBlue = (int) (invaderColor.getBlue()*255) + Configuration.COLOR_CHANGE;
        if(newRed < 255 && newGreen < 255 && newBlue < 255) {
            invaderColor = Color.rgb(newRed, newGreen, newBlue);
            ((Rectangle) this.getNode()).setFill(invaderColor);
        }

    }

    @Override
    public void healthCheck() {
        this.removeHP(1);
        darkenColor();
        System.out.println("DECREASE");
        if(this.getHP() <= 0){
            this.explode();
        }
    }

}

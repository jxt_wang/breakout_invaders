/**
 * Created by Justin Wang on 1/15/2017.
 * Level Classification
 *
 **/

package config;

public enum LevelClass {

    LEVEL_1(2), LEVEL_2(3), LEVEL_3(4), LEVEL_4(5), LEVEL_5(6), LEVEL_6(0);

    private int levelNum;

    LevelClass(int levelNum){
        this.levelNum = levelNum;
    }

    public int nextLevelVal(){
        return levelNum;
    }

    public int currentLevel(){ return Character.getNumericValue(this.toString().charAt(this.toString().length()-1)); }

    public String getName(){
        return (levelNum != 0) ? "Level " + (levelNum-1) : "Final Level";
    }

}

/**
 * Created by Justin Wang on 1/15/2017.
 * config.LevelData File
 * Provides Level Data
 *
 **/

package config;

import java.io.InputStream;
import java.util.*;

public class LevelData {

    /*Mapping Of Levels*/
    private Map<Integer, List<String>> levelMap;

    /*Level Classification*/
    private LevelClass levelClass;

    /*File Scanner*/
    private Scanner dataScanner;

    public LevelData(LevelClass levelClass){

        this.levelClass = levelClass;
        this.initializeMap();

    }

    public List<String> getLevelData(){

        switch (this.levelClass){
            case LEVEL_1:
                return levelMap.get(1);
            case LEVEL_2:
                return levelMap.get(2);
            case LEVEL_3:
                return levelMap.get(3);
            case LEVEL_4:
                return levelMap.get(4);
            case LEVEL_5:
                return levelMap.get(5);
            case LEVEL_6:
                return levelMap.get(6);
            default:
                return levelMap.get(1);
        }

    }

    public void initializeMap(){

        this.levelMap = new HashMap<Integer, List<String>>();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(Configuration.LEVEL_DATA_FILE);
        dataScanner = new Scanner(inputStream);

        List<String> nextLVL = new ArrayList<String>();

        for(int lvlX = 1; dataScanner.hasNextLine();){

            String nextLine = dataScanner.nextLine();

            if(!nextLine.equals(Configuration.NEXT_LVL)){

                nextLVL.add(nextLine);

            }
            else{

                levelMap.put(lvlX, new ArrayList<String>(nextLVL));
                nextLVL.clear();
                lvlX++;

            }

        }

    }

}
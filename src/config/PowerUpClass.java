/**
 * Created by Justin Wang on 1/15/2017.
 * PowerUp Classification
 *
 **/

package config;

public enum PowerUpClass {
    EMP, INVADERCIDE, SHIELD, ARTILLERY, NONE;
}
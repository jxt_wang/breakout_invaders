/**
 * Created by Justin Wang on 1/15/2017.
 * config.Configuration File
 * Provides ALL Configurations For Final Variables
 *
 **/

package config;

import javafx.scene.paint.Color;
import javafx.util.Duration;

public final class Configuration {

    /*TITLES*/
    public static final String TITLE = "Breakout Invaders!";
    public static final String GAME_OVER = "Game Over!";
    public static final String RESTART = "Restart";
    public static final String WIN = "You Won!";

    /*MAGIC NUMBERS*/
    public static final double FRAMES_PER_SECOND = 60.0;
    public static final Duration FRAME_TIME = Duration.millis((float)(1000/FRAMES_PER_SECOND));
    public static final int INV = -1;
    public static final int RAND_FREQ = 15;
    public static final int LIVES = 5;

    public static final int SHOT_COUNTER = 5;
    public static final int SHOT_TIME = 2;
    public static final int POWERUP_COUNT = 4;

    /*WINDOW SPECS*/
    public static final int WIDTH = 750;
    public static final int HEIGHT = 750;

    /*PADDLE SPECS*/
    public static final int PADDLE_START_WIDTH = 100;
    public static final int PADDLE_START_HEIGHT = 12;
    public static final double PADDLE_LEFT = -8;
    public static final double PADDLE_RIGHT = 8;
    public static final double RELEASE_SPEED = -8;
    public static final double MAX_BOUNCE = 45;

    /*SHIELD SPECS*/
    public static final int ARC = 10;
    public static final int SHIELD_STROKE = 2;
    public static final int SHIELD_BUFFER = 5;
    public static final int SHIELD_WIDTH = PADDLE_START_WIDTH + SHIELD_BUFFER*2;
    public static final int SHIELD_HEIGHT = PADDLE_START_HEIGHT + SHIELD_BUFFER*2;
    public static final int SHIELD_LIVES = 1;
    public static final int SHIELD_HP = 5;

    /*TEXT SPECS*/
    public static final int TEXT_X = WIDTH/30;
    public static final int TEXT_Y = HEIGHT - TEXT_X;

    /*BALL SPECS*/
    public static final int BALL_RADIUS = 8;

    /*INVADER SPECS*/
    public static final double INVADER_SPEED = 0.5;
    public static final double INVADER_VY = 30;
    public static final double INVADER_VX = 0.15;
    public static final int BUFFER = 5;

    /*SHOT SPECS*/
    public static final double SHOT_WIDTH = 5;
    public static final double SHOT_HEIGHT = 20;
    public static final double SHOT_SPEED = 4;

    /*FILE NAMES*/
    public static final String STANDARD_INVADER = "standard.png";
    public static final String SHIELD_INVADER = "shield.png";
    public static final String ASSAULT_INVADER = "assault.png";
    public static final String SUPPORT_INVADER = "support.png";
    public static final String LEVEL_DATA_FILE = "data.txt";
    public static final String BALL = "ball.png";
    public static final String DEFENDER = "defender.png";
    public static final String FONT = "MunroSmall.ttf";

    /*LEVEL DATA NAMES*/
    public static final String NEXT_LVL = "NEXTLVL";
    public static final String STANDARD_STRING = "ST";
    public static final String STANDARD_STRING_LIGHT = "SL";
    public static final String SHIELD_STRING = "SH";
    public static final String ASSAULT_STRING = "AS";
    public static final String SUPPORT_STRING = "SU";
    public static final String SUPPLY_STRING = "SD";
    public static final String INVADERCIDE = "IN";

    /*COLORS*/
    public static final Color GREY = Color.rgb(100,100,100);
    public static final Color DARK_GREY = Color.rgb(125,125,125);
    public static final Color LIGHT_GREY = Color.rgb(235,235,235);
    public static final Color GREEN = Color.rgb(50,140,45);
    public static final Color RED = Color.rgb(150,15,20);
    public static final Color BLUE = Color.rgb(15,50,80);
    public static final Color BRIGHT_BLUE = Color.rgb(15,205,210);
    public static final int COLOR_CHANGE = 30;

    /*RATIOS*/
    public static final double WIDTH_RATIO = 0.5;
    public static final double HEIGHT_RATIO = 0.4;

    /*HP*/
    public static final int STANDARD_HP = 1;
    public static final int SUPPORT_HP = 3;

    /*SPLASH SCREEN*/
    public static final String SPLASH = "Controls: \n\n\tLeft/Right Arrow Keys: Controls movement of the paddle.\n\n" +
            "\tSpacebar: Releases the ball if the ball has been reset. When released, the ball travels directly up.\n\n" +
            "\tUp: Activates Shield (5HP)\n" +
            "\tDown: Activates Power Up (if power up is available)\n" + "\tSHIFT: Launches Shot (5 Per Round)\n\n" +
            "Cheat Codes: \n\n" +
            "\t0. Main Menu\n" +
            "\t1. Next Level\n" +
            "\t2. Previous Level\n" +
            "\tU. 1UP life\n" +
            "\tD. Lose a life\n" +
            "\tP. Pause movement/game\n" +
            "\tR. Reset projectile\n\n" + "Block Types: \n\n\tStandard Invader (White): Invader that requires one hit to be destroyed. Does not have the ability to fire.\n" +
            "\n" +
            "\tAssault Invader (Red): Like the standard invader, it only requires one hit to be destroyed. However, this invader has the ability to \n\tfire small projectiles towards the player. These projectiles move vertically and can only be deflected by a player's shield. Its \n\tprojectiles can only harm the player's paddle.\n" +
            "\n" +
            "\tShield Invader (Blue): This invader carries a shield that prevents it from being killed by a direct hit to the side facing the player. \n\tHowever, if the invader is hit on any other side, it is destroyed.\n" +
            "\n" +
            "\tSupport Invader (Green): Invader that requires 3 hits to be killed.\n\n" +
            "PowerUps:\n" +
            "\n\t* EMP: Stops the invaders from advancing and freezes them in place for 10 seconds. After the 10-second period\n" +
            "        elapses, the invaders resume movement.\n\n" +
            "\t* Invadercide: When the ball hits an invader, all invaders of the same type remaining on the screen will be destroyed.\n\n" +
            "\t* Shield: An additional shield is granted to the user.\n\n" +
            "\t* Artillery: Allows the user to fire small projectiles (like the Assault Invaders) at the incoming invaders. \n\tThis ability lasts for 5 shots.";

}

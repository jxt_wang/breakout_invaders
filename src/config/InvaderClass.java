/**
 * Created by Justin Wang on 1/15/2017.
 * Invader Classification
 *
 **/

package config;

public enum InvaderClass {
    STANDARD, SHIELD, ASSAULT, SUPPORT, POWERUP, BLANK
}

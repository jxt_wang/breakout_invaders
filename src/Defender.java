/**
 * Created by Justin Wang on 1/15/2017.
 * Defender Class
 *
 **/

import config.Configuration;
import config.PowerUpClass;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;

public class Defender extends Entity{

    private boolean powerUpEnabled;

    private PowerUpClass powerUpClass;

    private int shotCount;

    public Defender(Group rootNode, double startX, double startY, double xVelocity, double yVelocity) {
        super(rootNode, startX, startY, xVelocity, yVelocity);
        this.setHP(10); this.powerUpEnabled = false; this.powerUpClass = PowerUpClass.NONE;
        this.render(); shotCount = Configuration.SHOT_COUNTER;
        this.getRootNode().getChildren().add(this.getNode());
    }

    public Defender(Group rootNode){
        this(rootNode, 0, 0, 0, 0);
    }

    public boolean shotEnabled(){
        return (shotCount > 0);
    }

    public void enablePowerUp(PowerUpClass powerUpClass){

        powerUpEnabled = true;
        this.powerUpClass = powerUpClass;

    }

    public void resetPowerUp(){ this.powerUpClass = PowerUpClass.NONE;}

    public boolean isPowerUpEnabled(){ return powerUpEnabled; }

    public void disablePowerUp(){ powerUpEnabled = false; }

    public PowerUpClass powerUpClass(){ return powerUpClass; }

    public void loseShot(){ shotCount--; }

    public int getShots(){ return shotCount; }

    /*PARENT FUNCTIONS*/

    //TODO: Implement rounded corners
    @Override
    public void render() {

        Rectangle displayRect = new Rectangle(){
            {
                setWidth(Configuration.PADDLE_START_WIDTH);
                setHeight(Configuration.PADDLE_START_HEIGHT);
                setFill(Configuration.GREY);
            }
        };

        //ImageView displayRect = new ImageView(new Image(getClass().getClassLoader().getResourceAsStream(Configuration.DEFENDER)));

        this.setNode(displayRect);
        this.setPosition();

    }

    @Override
    public void checkCollision(Entity entityX) {

        if(entityX instanceof Ball){

            double xIntersect = entityX.getMaxX() - (this.getMinX() + this.getWidth()/2);

            if(entityX.getNode().getBoundsInParent().intersects(this.getNode().getBoundsInParent())){
                double nIntersect = (xIntersect/(this.getWidth()/2));
                double bAngle = (nIntersect * Configuration.MAX_BOUNCE) + 90;

                entityX.setVelocity(Configuration.RELEASE_SPEED*Math.cos(Math.toRadians(bAngle)), Configuration.RELEASE_SPEED*Math.abs(Math.sin(Math.toRadians(bAngle))));
            }

        }

    }

    @Override
    public void healthCheck() {
        this.removeHP(1);
        if(this.getHP() <= 0){
            this.setDead(true);
        }
    }

}

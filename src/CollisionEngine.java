/**
 * Created by Justin Wang on 1/15/2017.
 * CollisionEngine for Breakout
 *
 **/

import config.Configuration;
import config.InvaderClass;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CollisionEngine {

    /*Main Elements*/
    private Defender mainDefender;
    private Ball mainBall;
    private EntityManager invManager;
    private EntityManager shotManager;
    private Shield mainShield;

    /*Stage Elements*/
    private Scene primaryScene;
    private List<KeyCode> inputEvents;
    private Group rootNode;
    private Group shotGroup;

    /*Movement Variables*/
    private boolean moveDown;
    private double movement;
    private boolean hasShield;

    private Level primaryLevel;
    private int counter = 0;

    /*Counter*/
    //private double shotCounter;

    public CollisionEngine(Level primaryLevel, Group rootNode, Defender mainDefender, Ball mainBall, Scene primaryScene, List<KeyCode> inputEvents, EntityManager invManager){

        this.mainDefender = mainDefender;
        this.mainBall = mainBall;
        this.invManager = invManager;
        this.shotManager = new EntityManager(shotGroup);
        this.primaryLevel = primaryLevel;

        this.primaryScene = primaryScene;
        this.inputEvents = inputEvents;
        this.rootNode = rootNode;
        this.shotGroup = new Group();

        this.rootNode.getChildren().add(shotGroup);

        this.moveDown = false; this.hasShield = false;
        this.movement = Configuration.INVADER_SPEED;

    }


    public void checkMove(){

        bounceCheck();
        checkBorders();
        zeroVelocity();
        moveShot();
        disableShield();

        if(inputEvents.contains(KeyCode.LEFT) && mainDefender.getMinX() >= 0){
            mainDefender.setVelocity(Configuration.PADDLE_LEFT, 0);
            if(hasShield){ mainShield.setVelocity(Configuration.PADDLE_LEFT, 0); }
            if(!mainBall.isReleased()) {mainBall.addVelocity(Configuration.PADDLE_LEFT, 0);}
        }

        if(inputEvents.contains(KeyCode.RIGHT) && (mainDefender.getMinX() + mainDefender.getWidth()) < primaryScene.getWidth()){
            mainDefender.addVelocity(Configuration.PADDLE_RIGHT, 0);
            if(hasShield){ mainShield.setVelocity(Configuration.PADDLE_RIGHT, 0); }
            if(!mainBall.isReleased()) {mainBall.addVelocity(Configuration.PADDLE_RIGHT, 0);}
        }

        mainBall.update();
        mainDefender.update();

        if(hasShield){
            mainShield.update();
        }

        if(inputEvents.contains(KeyCode.SPACE) && !mainBall.isReleased()){
            mainBall.setRelease(true);
        }


    }

    public void addShield(Shield mainShield){
        this.mainShield = mainShield;
        this.hasShield = true;
    }

    public void disableShield(){
        if(hasShield && mainShield.isDead()){
            mainShield.getRootNode().getChildren().remove(mainShield.getNode());
            hasShield = false;
        }
    }

    int count = 1;

    public void checkShot(){

        if(mainDefender.shotEnabled() && inputEvents.contains(KeyCode.SHIFT)){

            if(count%120 == 0) {

                Shot shotX = new Shot(shotGroup, mainDefender.getMinX() + mainDefender.getWidth() / 2, mainDefender.getMinY() - Configuration.SHOT_HEIGHT * 2, 0, -Configuration.SHOT_SPEED);
                shotX.setDefenderShot(true);
                shotManager.add(shotX);
                mainDefender.loseShot();

            }

            count++;

        }
        else{
            count = 0;
        }

    }

    public void moveShot(){
        for (Entity shotX : shotManager.getEntityList()) {
            shotX.update();
            if(shotX.getMinY() <= 0 || shotX.getMaxY() >= primaryScene.getHeight()){
                shotX.explode();
            }
        }

        shotManager.checkDead();
    }

    public void zeroVelocity(){

        mainDefender.setVelocity(0, 0);
        if(!mainBall.isReleased()){
            mainBall.setVelocity(0, 0);
        }

        if(hasShield){
            mainShield.setVelocity(0, 0);
        }

    }

    public void checkBorders(){

        if(mainBall.isReleased()){

            if ((mainBall.getMaxY() > primaryScene.getHeight() - (Configuration.BALL_RADIUS))){
                resetProjectile();
                primaryLevel.loseLife();
            }
            if ((mainBall.getMaxX() > (primaryScene.getWidth()  - (Configuration.BALL_RADIUS*1.5)) && mainBall.getVelocityX() > 0) ||
                    (mainBall.getMaxX() < (Configuration.BALL_RADIUS*1.5) && mainBall.getVelocityX() < 0)) {
                mainBall.invX();
            }
            if(mainBall.getMaxY() < ((2*Configuration.BALL_RADIUS))){
                mainBall.invY();
            }

        }

    }

    public void resetProjectile(){
        mainBall.setRelease(false);
        mainBall.setPosition(mainDefender.getMinX() + mainDefender.getNode().getBoundsInParent().getWidth()/2, mainDefender.getMinY() - Configuration.BALL_RADIUS*2);
    }

    public void bounceCheck(){
        mainDefender.checkCollision(mainBall);
    }

    public boolean getShieldEnabled(){ return hasShield; }

    public void collisionCheck(){

        for (Entity entityX : invManager.getEntityList()){
            entityX.checkCollision(mainBall);
            if(((Invader)entityX).invaderClass() == mainBall.getInvaderClass()){
                entityX.explode();
            }
        }

        for(Entity entityX : shotManager.getEntityList()){
            entityX.checkCollision(mainBall);
            entityX.checkCollision(mainDefender);
            if(hasShield) {
                entityX.checkCollision(mainShield);
            }

            for(Entity entityY : invManager.getEntityList()){
                entityX.checkCollision(entityY);
            }

        }

        invManager.checkDead();
        shotManager.checkDead();

    }

    public boolean moveInvader(){

        if(invManager.getRootNode().getBoundsInParent().getMaxX() < primaryScene.getWidth() && invManager.getRootNode().getBoundsInParent().getMinX() >= 0) {

            for (Node nodeX: invManager.getRootNode().getChildren()){
                nodeX.setTranslateX(nodeX.getTranslateX() + movement);
            }

        }
        else{

            movement *= -1;

            if(movement > 0 && moveDown){
                for (Node nodeX: invManager.getRootNode().getChildren()){
                    nodeX.setTranslateY(nodeX.getTranslateY() + Configuration.INVADER_VY);
                }
                movement += Configuration.INVADER_VX;
            }
            for (Node nodeX: invManager.getRootNode().getChildren()){
                nodeX.setTranslateX(nodeX.getTranslateX() + movement);

            }
            moveDown = !moveDown;
        }

        return (this.checkEnd(mainDefender.getMinY()));

    }

    public void pauseInvader(){
        counter++;
        if(counter >= 600){
            counter = 0;
            primaryLevel.disableEMP();
            mainDefender.disablePowerUp();
            mainDefender.resetPowerUp();
        }
    }

    public void generateShot(int randInt){

        if(randInt == 1 && !invManager.getEntityList().isEmpty()) {

            Entity entityX = invManager.getEntityList().get(ThreadLocalRandom.current().nextInt(0, invManager.getEntityList().size()));

            if (entityX instanceof Invader && ((Invader) entityX).attack()) {
                Shot shotX = new Shot(shotGroup, entityX.getMinX() + entityX.getWidth()/2, entityX.getMinY() + entityX.getHeight()/2, 0, Configuration.SHOT_SPEED);
                shotManager.add(shotX);
            }

        }

    }

    public boolean checkEnd(double endLine){

        for (Entity entityX : invManager.getEntityList()) {
            if (entityX instanceof Invader && ((Invader)entityX).invaderClass() != InvaderClass.POWERUP && entityX.getMaxY() >= endLine) {
                System.out.println("wtf");
                return true;
            }
        }

        return false;

    }

}
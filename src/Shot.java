/**
 * Created by Justin Wang on 1/15/2017.
 * Shot Class
 *
 **/

import config.Configuration;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;

public class Shot extends Entity{

    private boolean defenderShot;

    public Shot(Group rootNode, double startX, double startY, double xVelocity, double yVelocity){
        super(rootNode, startX, startY, xVelocity, yVelocity);
        this.defenderShot = false;
        this.render();
        this.getRootNode().getChildren().add(this.getNode());
    }

    public void setDefenderShot(boolean defenderShot){ this.defenderShot = defenderShot; ((Rectangle)this.getNode()).setFill(Configuration.BRIGHT_BLUE);}

    @Override
    public void render() {

        Rectangle displayRect = new Rectangle(){
            {
                setWidth(Configuration.SHOT_WIDTH);
                setHeight(Configuration.SHOT_HEIGHT);
                setFill(Configuration.RED);
            }
        };

        this.setNode(displayRect);
        this.setPosition();

    }

    @Override
    public void explode(){

        super.explode();
        this.getRootNode().getChildren().remove(this.getNode());

    }

    @Override
    public void checkCollision(Entity entityX) {

        if(entityX.getNode().getBoundsInParent().intersects(this.getNode().getBoundsInParent())) {

            if(entityX instanceof Shield && !defenderShot){
                entityX.healthCheck();
                this.explode();
            }
            else if (entityX instanceof Defender && !defenderShot) {
                entityX.healthCheck();
                this.explode();
            }
            if (entityX instanceof Ball && !defenderShot) {
                this.explode();
            }

            if(entityX instanceof Invader && defenderShot){
                entityX.healthCheck();
                this.explode();
            }

        }

    }

    @Override
    public void healthCheck() {

    }

}

/**
 * Created by Justin Wang on 1/15/2017.
 * Level Class
 * Primary Structure For All Levels Of The Game
 * <p>
 * <p>
 * TODO: HAVE A FLEXIBLE PADDLE WIDTH FOR POWERUPS
 * TODO: Seperate timeline for powerups (start when power up is activated)
 */

/**
 * TODO: HAVE A FLEXIBLE PADDLE WIDTH FOR POWERUPS
 * TODO: Seperate timeline for powerups (start when power up is activated)
 *
 */

import config.*;
import javafx.animation.KeyFrame;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Level {

    /*Defender Parameters*/
    private final double xStart = Configuration.WIDTH / 2 - Configuration.PADDLE_START_WIDTH / 2;
    private final double yStart = Configuration.HEIGHT - Configuration.PADDLE_START_HEIGHT * 6;
    /*Game Engine*/
    private GameEngine gameEngine;
    /*Level Generator*/
    private LevelData levelData;
    /*Collision Engine*/
    private CollisionEngine collisionEngine;
    /*Level Classification*/
    private LevelClass levelClass;
    private Text lifeText;
    private Text healthVal;
    private Text shieldVal;
    private Text shieldCount;
    private Text powerUp;
    /*Input Manager*/
    private List<KeyCode> inputEvents;
    /*Scene Elements*/
    private Stage primaryStage;
    private Scene primaryScene;
    private Group rootNode;
    private Group invaderGroup;
    private Font pixelFont;
    /*Entities*/
    private Defender mainDefender;
    private Ball mainBall;
    private EntityManager invManager;
    private Shield tempShield;
    /*Life Elements*/
    private int lifeCounter = Configuration.LIVES;//TODO: Change
    private Group lifeGroup;
    private int shieldCounter = Configuration.SHIELD_LIVES;
    /*Game Timeline*/
    private ParallelTransition primaryTimeline;

    /*Cheats*/
    private boolean paused = false;

    private boolean empEnabled = false;
    private Text shotText;

    public Level(Stage primaryStage, LevelClass levelClass, GameEngine gameEngine) {

        this.levelClass = levelClass;
        this.levelData = new LevelData(levelClass);
        this.primaryStage = primaryStage;
        this.gameEngine = gameEngine;

        this.rootNode = new Group();
        this.invaderGroup = new Group();
        this.lifeGroup = new Group();
        this.rootNode.getChildren().add(lifeGroup);
        this.primaryScene = new Scene(rootNode, Configuration.WIDTH, Configuration.HEIGHT);

        this.initEvents(primaryScene);
        this.initEntity(rootNode);
        this.initTimeline(mainDefender, mainBall, rootNode);
        this.initText();
        this.initLifeCounter(lifeCounter);

        this.primaryStage.setScene(this.primaryScene);
        this.begin(primaryTimeline);

    }

    public Level(Stage primaryStage, LevelClass levelClass, GameEngine gameEngine, int HP, int lifeCounter, int shieldCounter) {
        this(primaryStage, levelClass, gameEngine);
        this.shieldCounter = shieldCounter;
        mainDefender.setHP(HP);
        initLifeCounter(lifeCounter);
    }

    public void initLifeCounter(int lifeCounter) {

        this.lifeCounter = lifeCounter;

        lifeGroup.getChildren().clear();

        double start = lifeText.getX();
        double offset = lifeText.getBoundsInParent().getWidth() * 2;

        for (int i = 0; i < lifeCounter; i++) {

            Circle displayCircle = new Circle() {
                {
                    setRadius(Configuration.BALL_RADIUS);
                    setFill(Configuration.GREY);
                }
            };

            displayCircle.setCenterX(offset + start * i);
            displayCircle.setCenterY(lifeText.getY() - lifeText.getBoundsInParent().getHeight() / 4);

            lifeGroup.getChildren().add(displayCircle);

        }

    }

    public void initText() {

        pixelFont = Font.loadFont(getClass().getClassLoader().getResourceAsStream(Configuration.FONT), 25);
        lifeText = setText("Lives:", Configuration.TEXT_X, Configuration.TEXT_Y);
        healthVal = setText(Integer.toString(mainDefender.getHP()), primaryScene.getWidth() - Configuration.TEXT_X * 1.5, Configuration.TEXT_Y);
        //healthText = setText("HP:", Configuration.TEXT_X, Configuration.TEXT_Y);
        shieldVal = setText("N/A", primaryScene.getWidth() - Configuration.TEXT_X * 3, Configuration.TEXT_Y);
        shotText = setText(Integer.toString(mainDefender.getShots()), primaryScene.getWidth() - Configuration.TEXT_X * 6, Configuration.TEXT_Y);
        shieldCount = setText(Integer.toString(shieldCounter), primaryScene.getWidth() - Configuration.TEXT_X * 4.5, Configuration.TEXT_Y);
        powerUp = setText(mainDefender.powerUpClass().name(), primaryScene.getWidth() * 7 / 13, Configuration.TEXT_Y);
        rootNode.getChildren().addAll(lifeText, healthVal, shieldVal, shieldCount, powerUp, shotText);

    }

    public Text setText(String title, double x, double y) {

        Text text = new Text() {
            {
                setText(title);
                setFont(pixelFont);
                setX(x);
                setY(y);

            }
        };

        return text;

    }

    /*INITIALIZE FUNCTIONS*/

    public void initEvents(Scene primaryScene) {

        this.inputEvents = new ArrayList<KeyCode>();

        primaryScene.setOnKeyPressed(
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        KeyCode eventCode = keyEvent.getCode();
                        if (!inputEvents.contains(eventCode)) {
                            inputEvents.add(eventCode);
                        }
                        cheatCodes();
                        abilities();
                        initDebugger(eventCode);
                    }
                }
        );

        primaryScene.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        KeyCode eventCode = keyEvent.getCode();
                        if (inputEvents.contains(eventCode)) {
                            inputEvents.remove(eventCode);
                        }
                    }
                }
        );

    }

    public void initDebugger(KeyCode keyCode) {

        switch (keyCode) {
            case DIGIT1:
                previousLevel();
                break;
            case DIGIT2:
                nextLevel();
                break;
            case DIGIT0:
                primaryTimeline.stop();
                gameEngine.setSplash(primaryStage);
                break;
            default:
                break;
        }

    }

    public void initTimeline(Defender mainDefender, Ball mainBall, Group rootNode) {

        primaryTimeline = new ParallelTransition();

        Timeline entityTimeline = new Timeline();
        entityTimeline.setCycleCount(Timeline.INDEFINITE);
        entityTimeline.setAutoReverse(false);

        //Timeline shotTimeline = addTimeline();
        //primaryTimeline.getChildren().add(shotTimeline);

        final KeyFrame main_frame = new KeyFrame(Configuration.FRAME_TIME,
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {

                        engineUpdate();

                        textUpdate();

                        defenderUpdate();

                        shieldUpdate();
                        if (isEnd()) {
                            nextLevel();
                        }

                    }

                });


        entityTimeline.getKeyFrames().add(main_frame);
        primaryTimeline.getChildren().add(entityTimeline);

    }

    public void engineUpdate() {

        collisionEngine.checkMove();
        collisionEngine.collisionCheck();
        collisionEngine.checkShot();

        if (!empEnabled) {
            if (collisionEngine.moveInvader()) {
                end(primaryTimeline);
            }
            collisionEngine.generateShot(ThreadLocalRandom.current().nextInt(0, Configuration.RAND_FREQ + 1));
        } else {
            collisionEngine.pauseInvader();
        }

    }

    public void defenderUpdate() {
        if (mainDefender.isDead()) {
            end(primaryTimeline);
        }
    }

    public void textUpdate() {

        shieldCount.setText(Integer.toString(shieldCounter));

        healthVal.setText(Integer.toString(mainDefender.getHP()));

        shotText.setText(Integer.toString(mainDefender.getShots()));

        if (collisionEngine.getShieldEnabled()) {
            shieldVal.setText(Integer.toString(tempShield.getHP()));
        }

        if (mainDefender.isPowerUpEnabled() && mainDefender.powerUpClass() != PowerUpClass.SHIELD) {
            powerUp.setText(mainDefender.powerUpClass().name());
            powerUp.setStroke(Color.RED);
        } else {
            powerUp.setStroke(Color.TRANSPARENT);
        }

    }

    public void shieldUpdate() {

        if (mainDefender.isPowerUpEnabled() && mainDefender.powerUpClass() == PowerUpClass.SHIELD) {
            mainDefender.disablePowerUp();
            shieldCounter++;
        }

    }

    public void cheatCodes() {

        if (inputEvents.contains(KeyCode.P)) {
            if (paused) {
                primaryTimeline.play();
                primaryTimeline.play();
            } else {
                primaryTimeline.pause();
            }
            paused = !paused;
        }

        if (inputEvents.contains(KeyCode.R)) {
            collisionEngine.resetProjectile();
        }

        if (inputEvents.contains(KeyCode.D)) {
            loseLife();
        }

        if (inputEvents.contains(KeyCode.U)) {
            gainLife();
        }

    }

    public void abilities() {

        if (inputEvents.contains(KeyCode.UP)) {
            if (shieldCounter > 0 && !collisionEngine.getShieldEnabled()) {
                tempShield = new Shield(rootNode, mainDefender.getMinX() - Configuration.SHIELD_BUFFER, mainDefender.getMinY() - Configuration.SHIELD_BUFFER, 0, 0);
                this.collisionEngine.addShield(tempShield);
                shieldCounter--;
            }
        }

        if (mainDefender.isPowerUpEnabled() && inputEvents.contains(KeyCode.DOWN)) {
            switch (mainDefender.powerUpClass()) {

                case INVADERCIDE:
                    mainBall.setInvadercide(true);
                    mainDefender.disablePowerUp();
                    mainDefender.resetPowerUp();
                    break;
                case EMP:
                    empEnabled = true;
                    break;
                default:
                    break;

            }
        }

    }

    public void disableEMP() {
        empEnabled = false;
    }

    public void initEntity(Group rootNode) {

        this.mainDefender = new Defender(rootNode, xStart, yStart, 0, 0);
        this.mainBall = new Ball(rootNode, mainDefender.getMinX() + mainDefender.getWidth() / 2, mainDefender.getMinY() - Configuration.BALL_RADIUS * 2, 0, 0);
        this.generateLayout(levelData, primaryScene, rootNode);

        this.collisionEngine = new CollisionEngine(this, rootNode, mainDefender, mainBall, primaryScene, inputEvents, invManager);

    }

    public void loseLife() {

        if (lifeCounter > 0) {
            this.lifeCounter--;
        }

        if (!lifeGroup.getChildren().isEmpty()) {
            lifeGroup.getChildren().remove(lifeGroup.getChildren().size() - 1);
        }

        if (lifeCounter <= 0) {
            end(primaryTimeline);
        }

    }

    public void gainLife() {

        if (lifeCounter < 10) {

            double start = lifeText.getX();
            double offset = lifeText.getBoundsInParent().getWidth() * 2;

            this.lifeCounter++;
            Circle displayCircle = new Circle() {
                {
                    setRadius(Configuration.BALL_RADIUS);
                    setFill(Configuration.GREY);
                }
            };

            displayCircle.setCenterX(offset + start * lifeGroup.getChildren().size());
            displayCircle.setCenterY(lifeText.getY() - lifeText.getBoundsInParent().getHeight() / 4);
            lifeGroup.getChildren().add(displayCircle);

        }
    }

    public boolean isEnd() {

        boolean endLevel = true;

        for (Entity entityX : invManager.getEntityList()) {
            if (entityX instanceof Invader && !(entityX instanceof PowerUp)) {
                endLevel = false;
            }
        }

        return endLevel;

    }

    public void nextLevel() {

        primaryTimeline.stop();

        int lvl = levelClass.nextLevelVal();

        if (lvl == 0) {
            gameEngine.end(Configuration.WIN);
            return;
        }

        String levelName = "LEVEL_" + lvl;

        gameEngine.setLevel(primaryStage, LevelClass.valueOf(levelName), mainDefender.getHP(), lifeCounter, shieldCounter + 1);

    }

    public void previousLevel() {

        primaryTimeline.stop();

        int lvl = this.levelClass.currentLevel() - 1;

        if (lvl == 0) {
            gameEngine.setSplash(primaryStage);
            return;
        }

        String levelName = "LEVEL_" + lvl;

        gameEngine.setLevel(primaryStage, LevelClass.valueOf(levelName), mainDefender.getHP(), lifeCounter, shieldCounter);

    }

    public void generateLayout(LevelData levelData, Scene primaryScene, Group rootNode) {

        List<String> levelList = levelData.getLevelData();
        this.invManager = new EntityManager(invaderGroup);

        int countRow = 1;
        for (String rowX : levelList) {

            String[] splitRow = rowX.split("\\s+");

            int countIndex = 1;
            for (String itemX : splitRow) {

                InvaderClass invaderClass;
                Invader nextInvader;

                switch (itemX) {

                    case Configuration.STANDARD_STRING:
                        invaderClass = InvaderClass.STANDARD;
                        break;
                    case Configuration.SHIELD_STRING:
                        invaderClass = InvaderClass.SHIELD;
                        break;
                    case Configuration.ASSAULT_STRING:
                        invaderClass = InvaderClass.ASSAULT;
                        break;
                    case Configuration.SUPPORT_STRING:
                        invaderClass = InvaderClass.SUPPORT;
                        break;
                    case Configuration.SUPPLY_STRING:
                    case Configuration.INVADERCIDE:
                        invaderClass = InvaderClass.POWERUP;
                        break;
                    case Configuration.STANDARD_STRING_LIGHT:
                        invaderClass = InvaderClass.STANDARD;
                        break;
                    default:
                        invaderClass = InvaderClass.BLANK;
                        break;

                }

                if (invaderClass == InvaderClass.POWERUP) {
                    if (itemX.equals(Configuration.INVADERCIDE)) {
                        nextInvader = new PowerUp(mainDefender, invaderGroup, primaryScene, splitRow.length, countIndex, countRow, PowerUpClass.INVADERCIDE);
                    } else {
                        nextInvader = new PowerUp(mainDefender, invaderGroup, primaryScene, splitRow.length, countIndex, countRow);
                    }
                } else {
                    nextInvader = new Invader(invaderGroup, invaderClass, primaryScene, splitRow.length, countIndex, countRow);
                    if (itemX.equals(Configuration.STANDARD_STRING_LIGHT)) {
                        ((Rectangle) nextInvader.getNode()).setFill(Configuration.LIGHT_GREY);
                    }
                }

                invManager.add(nextInvader);
                countIndex++;

            }

            countRow++;

        }

        rootNode.getChildren().add(invaderGroup);

    }

    /*START & STOP*/

    public void begin(ParallelTransition primaryTimeline) {
        this.primaryTimeline.setCycleCount(Timeline.INDEFINITE);
        primaryTimeline.play();
    }

    public void end(ParallelTransition primaryTimeline) {

        primaryTimeline.stop();

        gameEngine.end(Configuration.GAME_OVER);

    }

    /*RETURN FUNCTIONS*/

    public Scene getScene() {
        return primaryScene;
    }

}
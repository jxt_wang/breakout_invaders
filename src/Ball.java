/**
 * Created by Justin Wang on 1/15/2017.
 * Projectile Class
 *
 **/

import config.Configuration;
import config.InvaderClass;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;

//TODO: CIRCULAR BOUNDING BOX FOR BALL

public class Ball extends Entity{

    /*Released Check*/
    private boolean isReleased;

    private boolean invadercide;

    private InvaderClass invaderClass;

    public Ball(Group rootNode, double startX, double startY, double xVelocity, double yVelocity){
        super(rootNode, startX, startY, xVelocity, yVelocity);
        this.render(); invadercide = false; invaderClass = InvaderClass.BLANK;
        this.getRootNode().getChildren().add(this.getNode());
    }

    public Ball(Group rootNode){
        this(rootNode, 0, 0, 0, 0);
    }

    public void setInvadercide(boolean invadercide){ this.invadercide = invadercide; }

    public void setInvader(InvaderClass invaderClass){ this.invaderClass = invaderClass; }

    public boolean getInvadercide(){ return invadercide; }

    public InvaderClass getInvaderClass(){ return invaderClass; }

    public void setRelease(boolean isReleased){

        double releaseSpeed = (isReleased) ? Configuration.RELEASE_SPEED : 0;
        this.setVelocity(0, releaseSpeed);
        this.isReleased = isReleased;

    }

    public void invX(){ this.setVelocityX(this.getVelocityX() * Configuration.INV); }

    public void invY(){ this.setVelocityY(this.getVelocityY() * Configuration.INV); }

    public boolean isReleased(){
        return isReleased;
    }

    /*PARENT FUNCTIONS*/

    @Override
    public void render() {

        Circle displayCircle = new Circle(){
            {
                setRadius(Configuration.BALL_RADIUS);
                setFill(Configuration.GREY);
            }
        };

        this.setNode(displayCircle);
        this.setPosition();
    }

    //TODO: check collision with bullet
    @Override
    public void checkCollision(Entity entityX) {

    }

    @Override
    public void healthCheck() {

    }

}

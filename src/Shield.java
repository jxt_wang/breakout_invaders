/**
 * Created by Justin Wang on 1/15/2017.
 * Shield Class
 *
 **/

import config.Configuration;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Shield extends Entity{

    public Shield(Group rootNode, double startX, double startY, double xVelocity, double yVelocity){
        super(rootNode, startX, startY, xVelocity, yVelocity);
        this.setHP(Configuration.SHIELD_HP);
        this.render();
        this.getRootNode().getChildren().add(this.getNode());
    }

    @Override
    public void render() {

        Rectangle displayRect = new Rectangle(){
            {
                setWidth(Configuration.SHIELD_WIDTH);
                setHeight(Configuration.SHIELD_HEIGHT);
                setFill(Color.TRANSPARENT);
                setStroke(Configuration.BRIGHT_BLUE);
                setStrokeWidth(Configuration.SHIELD_STROKE);
                setArcHeight(Configuration.ARC);
                setArcWidth(Configuration.ARC);
            }
        };

        this.setNode(displayRect);
        this.setPosition();

    }

    @Override
    public void explode(){

        super.explode();
        this.getRootNode().getChildren().remove(this.getNode());

    }

    @Override
    public void checkCollision(Entity entityX) {

    }

    @Override
    public void healthCheck() {
        this.removeHP(1);
        System.out.println(this.getHP());
        if(this.getHP() <= 0){
            this.setDead(true);
        }
    }
}

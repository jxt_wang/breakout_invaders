/**
 * Created by Justin Wang on 1/15/2017.
 * Main UI Interface for Breakout
 *
 **/

/**
 * TODO: Integrate keyboard level switching
 * TODO: Integrate level ending/begining
 * TODO: Integrate PowerUps and activation
 * TODO: Display player stats
 *
 * TODO: Implement explosions
 *
 */

import config.Configuration;
import config.LevelClass;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GameEngine extends Application {

    /*Stage Elements*/
    private Stage primaryStage;
    private GameEngine gameEngine;

    /*Font*/
    private Font pixelFont;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        this.gameEngine = this;
        pixelFont = Font.loadFont(getClass().getClassLoader().getResourceAsStream(Configuration.FONT), 50);

        this.initStage(primaryStage);
        primaryStage.setResizable(false);

        this.setSplash(primaryStage);
        primaryStage.show();

    }

    public void initStage(Stage primaryStage){

        this.primaryStage = primaryStage;
        primaryStage.setTitle(Configuration.TITLE);

    }

    public void setLevel(Stage primaryStage, LevelClass levelClass){

        Level nextLevel = new Level(primaryStage, levelClass, gameEngine);
        primaryStage.setScene(nextLevel.getScene());
        primaryStage.setTitle(levelClass.getName());

    }

    public void setLevel(Stage primaryStage, LevelClass levelClass, int HP, int lifeCounter, int shieldCounter){

        Level nextLevel = new Level(primaryStage, levelClass, gameEngine, HP, lifeCounter, shieldCounter);
        primaryStage.setScene(nextLevel.getScene());
        primaryStage.setTitle(levelClass.getName());

    }

    public void setSplash(Stage primaryStage){

        primaryStage.setTitle(Configuration.TITLE);

        VBox splashNode = new VBox();
        Scene splashScene = new Scene(splashNode, Configuration.WIDTH, Configuration.HEIGHT);
        Hyperlink startLink = new Hyperlink(Configuration.TITLE){{setOnAction(e -> setLevel(primaryStage, LevelClass.LEVEL_1)); setFont(pixelFont);}};

        TextArea textArea = new TextArea();
        textArea.setFont(Font.loadFont(getClass().getClassLoader().getResourceAsStream(Configuration.FONT), 13));
        textArea.setPrefRowCount(50);

        textArea.setText(Configuration.SPLASH);

        splashNode.getChildren().addAll(startLink, textArea);
        splashNode.setAlignment(Pos.CENTER);
        primaryStage.setScene(splashScene);

    }

    public void end(String TITLE){

        VBox endNode = new VBox();
        Scene endScene = new Scene(endNode, Configuration.WIDTH, Configuration.HEIGHT);
        Text endGameText = new Text(){{setFont(pixelFont); setText(TITLE);}};
        Hyperlink restartLink = new Hyperlink(Configuration.RESTART){{setOnAction(e -> setSplash(primaryStage)); setFont(pixelFont);}};
        endNode.getChildren().addAll(endGameText, restartLink);
        endNode.setAlignment(Pos.CENTER);
        primaryStage.setScene(endScene);

    }

}

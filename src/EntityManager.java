/**
 * Created by Justin Wang on 1/15/2017.
 * EntityManager Class
 *
 **/

import config.InvaderClass;
import javafx.scene.Group;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EntityManager{

    private ArrayList<Entity> entityList;

    private Group rootNode;

    public EntityManager(Group rootNode){ this.entityList = new ArrayList<Entity>(); this.rootNode = rootNode; }

    public EntityManager(List<Entity> entityList, Group rootNode){ this.entityList = new ArrayList<Entity>(entityList); this.rootNode = rootNode; }

    public Group getRootNode(){ return rootNode; }

    public ArrayList<Entity> getEntityList(){
        return entityList;
    }

    public ArrayList<Node> getNodeList(){

        ArrayList<Node> nodeList = new ArrayList<Node>();

        for(Entity entityX : entityList){
            nodeList.add(entityX.getNode());
        }

        return nodeList;

    }

    public void add(Entity entityX){
        entityList.add(entityX);
    }

    public void checkDead(){

        Iterator<Entity> entityIterator = entityList.iterator();

        while(entityIterator.hasNext()){

            Entity entityX = entityIterator.next();

            if(entityX.isDead()){
                entityIterator.remove();
            }
        }

    }

}
